import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController, ToastController, AlertController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/*
 Generated class for the Login component.

 See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 for more info on Angular 2 Components.
 */
@Component({
    selector: 'main-onawa-reset-password',
    templateUrl: 'reset-password.html'
})
export class MainOnawaResetPasswordComponent {
    form: FormGroup;

    @Input('options')
    options: any;

    @Input('service')
    service: any;

    @Output('addResetPasswordEvent')
    addResetPasswordEvent = new EventEmitter<any>();

    constructor(
        formBuilder: FormBuilder,
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        private translate: TranslateService
    ) {

        this.form = formBuilder.group({
            resetEmail: [ '', Validators.compose([ Validators.required ]) ],
        });
    }

    close(): void {
        this.navCtrl.pop();
    }

    validate(): boolean {
        if (this.form.valid) {
            return true;
        }

        let msg = '';

        if (this.form.controls[ 'resetEmail' ].hasError('required')) {
            this.translate.get('global.validate', {value: 'E-mail'}).subscribe((res: string) => {
                msg = res;
            });
        }

        const toast = this.toastCtrl.create({
            message: msg,
            duration: 3001,
            position: 'top'
        });

        toast.present();

        return false;
    }

    resetPassword(formData: any): void {
        if (!this.validate()) {
            return;
        }

        const loading = this.loadingCtrl.create();
        loading.present();

        this.service.resetPassword(formData.resetEmail).then((response) => {
            loading.dismiss();

            this.addResetPasswordEvent.emit(response);
        }).catch((ex) => {
            let response = JSON.parse(ex._body);

            this.translate.get('global.alert').subscribe((res: string) => {
                let alert = this.alertCtrl.create({
                    title: res,
                    subTitle: response.message,
                    buttons: ['OK']
                });
                alert.present();
            });

            loading.dismiss();
        });
    }
}
