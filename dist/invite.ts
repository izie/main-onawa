import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController, ToastController, AlertController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/*
 Generated class for the Login component.

 See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 for more info on Angular 2 Components.
 */
@Component({
    selector: 'main-onawa-invite',
    templateUrl: 'invite.html'
})
export class MainOnawaInviteComponent {
    form: FormGroup;

    @Input('options')
    options: any;

    @Input('service')
    service: any;

    @Output('addLoginEvent')
    addLoginEvent = new EventEmitter<any>();

    @Output('addEnviteEvent')
    addEnviteEvent = new EventEmitter<any>();

    constructor(
        formBuilder: FormBuilder,
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        private translate: TranslateService
    ) {

        this.form = formBuilder.group({
            code: [ '', Validators.compose([ Validators.required ]) ],
        });
    }

    close(): void {
        this.navCtrl.pop();
    }

    validate(): boolean {
        if (this.form.valid) {
            return true;
        }

        let msg = '';

        if (this.form.controls[ 'code' ].hasError('required')) {
            this.translate.get('global.validate', {value: 'Código'}).subscribe((res: string) => {
                msg = res;
            });
        }

        const toast = this.toastCtrl.create({
            message: msg,
            duration: 3001,
            position: 'top'
        });

        toast.present();

        return false;
    }

    inviteCode(formData: any): void {
        if (!this.validate()) {
            return;
        }

        const loading = this.loadingCtrl.create();
        loading.present();

        this.service.envite({ code: formData.code }).then((response) => {
            loading.dismiss();

            this.addEnviteEvent.emit(response);
        }).catch((ex) => {
            let response = JSON.parse(ex._body);

            this.translate.get('global.alert').subscribe((res: string) => {
                let alert = this.alertCtrl.create({
                    title: res,
                    subTitle: response.message,
                    buttons: ['OK']
                });
                alert.present();
            });

            loading.dismiss();
        });
    }

}
